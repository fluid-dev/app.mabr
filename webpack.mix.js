let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Set project paths
let localDomain = 'app.mabr.test';
let resourcesPath  = `resources`;
let publicPath  = `public`;

mix
// Suppress success messages
.disableSuccessNotifications()

// Compile Javascript (ES6)
.js(`${resourcesPath}/js/app.js`, `${publicPath}/js`)

// Compile Sass
.fastSass(`${resourcesPath}/scss/app.scss`, `${publicPath}/css`, {
  includedPaths: ['node_modules']
})
.sourceMaps()

// Setup BrowserSync
.browserSync({
  proxy: 'https://' + localDomain,
  host: localDomain,
  notify: false,
  open: false,
  reloadOnRestart: true,
  injectChanges: true,
  files: [
    // `**/*.php`,
    `${publicPath}/**/*.js`,
    `${publicPath}/**/*.css`
  ],
  https: {
    key: '/Users/zack/.config/valet/Certificates/app.mabr.test.key',
    cert: '/Users/zack/.config/valet/Certificates/app.mabr.test.crt'
  }
})

// Setup versioning (cache-busting)
if (mix.inProduction()) {
  mix.version();
  mix.babel(`${publicPath}/js/main.js`, `${publicPath}/js/main.js`);
}
