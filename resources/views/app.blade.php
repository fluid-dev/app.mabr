<!DOCTYPE html>
<html class="no-js" lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}">
    <link rel="apple-touch-icon" href="{{ asset('mobile-icon.png') }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
  	{{-- <!-- CSRF Token -->
  	<meta name="csrf-token" content="{{ csrf_token() }}"> --}}
  	<!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,600" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    {{-- Initialize Froala Editor key --}}
    <script id="fr-fek">try{(function (k){localStorage.FEK=k;t=document.getElementById('fr-fek');t.parentNode.removeChild(t);})('{{ env('FROALA_KEY') }}')}catch(e){}</script>
  </head>

  <body>
    <div id="office-action">
      <oa-navbar v-if="$auth.check()"></oa-navbar>
      <oa-topbar v-if="$auth.check()"></oa-topbar>

      <main
      v-if="$auth.ready()"
      :class="{ 'app' : $auth.check(), 'auth' : !$auth.check() }"
      class="view">
        <loading v-show="loading"></loading>
        <router-view></router-view>
      </main>
      <oa-infobar v-if="$auth.check()"></oa-infobar>
    </div>

  	<!-- Scripts -->
  	<script src="{{ mix('js/app.js') }}"></script>
  </body>
</html>
