@component('mail::message')
# Client: [{{ $matter->client->name }}]({{ url("/clients/{$matter->client->id}") }})


## New Matter Details
@component('mail::panel')
### Type
{{ $matter->type_name }}

### Title
{{ $matter->title }}

### Responsible Attorney
{{ $matter->responsibleAttorney->full_name }}

### Working Attorney
{{ $matter->workingAttorney->full_name }}

### Remarks
{{  $matter->remarks }}
@endcomponent

{{-- If it's a prosecution matter --}}
@if ($matter->type_class === 2)

## New Patent Prosecution Information
@component('mail::panel')
### Inventors
{{ $matter->inventors }}

### Client Reference Number
{{ $matter->reference_number }}

### Patent Country
{{ $matter->patent_country }}

### Patent Type
{{ $matter->patent_type }}

### Pattent Application Type
{{ $matter->patent_application_type }}

### Target Filing Date
{{ $matter->target_filing_date->format('m/d/Y') }}
@endcomponent

@endif

{{-- If it's a trademark matter --}}
@if ($matter->type_class === 3)

## New Trademark Registration Information
@component('mail::panel')
### Mark
{{ $matter->mark }}

### Class
{{ $matter->class }}

### Mark Country
{{ $matter->mark_country }}
@endcomponent

@endif

## Conflicts Check
@component('mail::panel')
### Narrative Summary
{{ $matter->narrative_summary }}

### Adverse Party/Parties
{{ $matter->adverse_party }}

### Nature of Representation
{{ $matter->representation }}

### Keywords
{{ $matter->keywords }}
@endcomponent

@component('mail::button', ['url' => url("/matters/{$matter->id}")])
View Matter
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
