import Vue from 'vue';

export default function guard(to, from, next) {
  if (Vue.auth.user().id == to.params.id || Vue.auth.check(['admin', 'hr', 'marketing'])) {
    return next();
  } else {
    return next({name: 'error403'});
  }
};
