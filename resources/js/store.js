// Import VueX
import Vuex from 'vuex';
import axios from 'axios'

// Global store configuration
export default new Vuex.Store({
  state: {
    loading: false,
    userMenuOpen: false,
    infoBarVisible: false,
    navBarVisible: false,
    settings: [],
    resources: []
  },

  getters: {
    //
  },

  mutations: {
    setLoading(state, status) {
      state.loading = status;
    },

    setSettings(state, settings) {
      state.settings = settings;
    },

    setResources(state, resources) {
      state.resources = resources;
    },

    updateSetting(state, data) {
      state.settings[data.key] = data.value;
    },

    toggleUserMenu(state) {
      state.userMenuOpen = !state.userMenuOpen;

      $('ul.user-menu').slideToggle();
    },

    toggleInfobar(state) {
      state.infoBarVisible = !state.infoBarVisible;

      if (state.navBarVisible) {
        state.navBarVisible = false;
      }
    },

    toggleNavbar(state) {
      state.navBarVisible = !state.navBarVisible;

      if (state.infoBarVisible) {
        state.infoBarVisible = false;
      }
    }
  },

  actions: {
    refreshResources(context) {
      axios.get('navbar')
      .then((res) => {
        context.commit('setResources', res.data.departments);
      })
    }
  }
});
