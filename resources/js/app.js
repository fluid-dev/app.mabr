import './bootstrap';
import router     from './router';
import store      from './store';
import Vue        from 'vue';
import { mapState } from 'vuex';

// Boot up app
let app = new Vue({
  el: '#office-action',
  computed: {
    ...mapState(['loading']),
    appReady() {
      return this.$auth.check();
    }
  },
  router,
  store,
  watch: {
    appReady: function(ready) {
      if (ready) {
        this.$http.get('settings')
        .then((res) => {
          this.$store.commit('setSettings', res.data.settings);
        });
      }
    }
  },

  mounted() {
    // Froala Editor invalid styles
    $(document)
    .on('invalid.zf.abide', (e) => {
      let input = $(e.target);
      if (input.is('textarea')) {
        let editor = input.prev();
        $('.fr-wrapper', editor).addClass('is-invalid-input');
      }
    })
    .on('valid.zf.abide', (e) => {
      let input = $(e.target);
      if (input.is('textarea')) {
        let editor = input.prev();
        $('.fr-wrapper', editor).removeClass('is-invalid-input');
      }
    });

    this.$router.afterEach(() => {
      // Toggle sidebar/navbar on route change
      if (Foundation.MediaQuery.current === 'small') {
        if ($('aside.infobar').hasClass('is-visible')) {
          $('aside.infobar').removeClass('is-visible');
        }

        if ($('nav.navbar').hasClass('is-visible')) {
          $('nav.navbar').removeClass('is-visible');
        }
      }
    })
  }
})
