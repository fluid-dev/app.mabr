// Import libraries
import './vendor/foundation';
import _       from 'lodash';
import $       from 'jquery';
import axios   from 'axios';
import swal    from 'sweetalert';
import select2 from 'select2/dist/js/select2.full.js';
import 'fullcalendar/dist/fullcalendar.js';
import 'fullcalendar/dist/gcal.js';
import router  from './router';

// Vue Dependencies
import Vue          from 'vue';
import Vuex         from 'vuex';
import VueRouter    from 'vue-router';
import VueAxios     from 'vue-axios';
import VueToast     from 'vue-izitoast';
import vue2Dropzone from 'vue2-dropzone';
import Datepicker   from 'vuejs-datepicker';
import VueFroala    from 'vue-froala-wysiwyg';
import VueSlick     from 'vue-slick';
import VueUtil     from './classes/Util';
require('froala-editor/js/froala_editor.pkgd.min');

// Vue Filters
import DotDate   from './filters/DotDate';
import SlashDate from './filters/SlashDate';
import Truncate  from './filters/Truncate';
import CommaSeparated from './filters/CommaSeparated';

// Attach jQuery to the window object
try {
  window.$ = window.jQuery = $;
} catch (e) {}

// Setup the Base URL for all API requests
let baseURL = `${location.protocol}//${location.hostname}`;

// If there's a port (Browsersync) add it
if (location.port) {
  baseURL = baseURL + ':' + location.port;
}

// Set the Axios base URL
axios.defaults.baseURL = `${baseURL}/api`;

// Add routes to Vue Router
Vue.router = router;

// Register Vue Plugins
Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueFroala);
Vue.use(VueAxios, axios);
Vue.use(VueUtil);
Vue.use(VueToast, {
  position: 'topRight',
  progressBar: false,
  timeout: 5000,
  closeOnClick: true
});

// Configure Vue Auth settings
Vue.use(require('@websanova/vue-auth'), {
  auth  : require('@websanova/vue-auth/drivers/auth/bearer.js'),
  http  : require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
  parseUserData: function (data) {
    return data;
  },
  forbiddenRedirect: { path: '/unauthorized' },
  notFoundRedirect: { path: '/not-found' },
  azureOauth2Data: {
    url: 'https://login.microsoftonline.com/common/oauth2/authorize',
    clientId: process.env.MIX_AZURE_CLIENT_ID,
    redirect: function () {
      return baseURL + '/oauth/callback/azure'
    }
  },
  azureData: {
    method: 'POST',
    redirect: '/',
    url: '/oauth/callback/azure'
  },
});

// Register Global Filters
Vue.filter('dotDate', DotDate);
Vue.filter('slashDate', SlashDate);
Vue.filter('truncate', Truncate);
Vue.filter('commaSeparated', CommaSeparated);

// Link Directive for Anchor-link behavior
Vue.directive('link', (el, binding) => {
  el.onclick = () => {
    window.open(`//${binding.value}`, '_blank');
  }
});

// Register Global Components
Vue.component('vue-slick', VueSlick);
Vue.component('vue-datepicker', Datepicker);
Vue.component('vue-dropzone', vue2Dropzone);
Vue.component('vue-select', require('./components/VueSelect'));
Vue.component('zf-abide', require('./components/foundation/Abide'));
Vue.component('zf-dropdown', require('./components/foundation/Dropdown'));
Vue.component('zf-pagination', require('./components/foundation/Pagination'));
Vue.component('zf-reveal', require('./components/foundation/Reveal'));
Vue.component('zf-switch', require('./components/foundation/Switch'));
Vue.component('zf-tabs', require('./components/foundation/Tabs'));
Vue.component('oa-navbar', require('./components/nav/NavBar'));
Vue.component('oa-infobar', require('./components/nav/InfoBar'));
Vue.component('oa-topbar', require('./components/nav/TopBar'));
Vue.component('five-dots', require('./components/FiveDots'));
Vue.component('loading', require('./components/Loading'));
Vue.component('oa-logo', require('./components/OALogo'));
Vue.component('three-dots', require('./components/ThreeDots'));
Vue.component('checkbox', require('./components/Checkbox'));
Vue.component('radio', require('./components/Radio'));
