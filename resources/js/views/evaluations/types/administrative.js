export default {
  groups: [
    {
      title: 'Technical Professional Skills',
      items: [
        {
          title: 'Quality Of Work',
          questions: [
            'Produces thorough, accurate and consistent work product.',
            'Work product includes proper format, content and style.',
            'Proofreading and accuracy.',
            'Pays attention to details.',
            'Conformity with established procedures.',
            'Ability to communicate points and ideas clearly, effectively, and succinctly.',
            'Professional judgment.'
          ]
        },
        {
          title: 'Production Level',
          questions: [
            'Consistently completes assignments in a timely manner.',
            'Meets deadlines.',
            'Shoulders share of workload.',
            'Efficient time management skills.',
            'Ability to revise priorities in order to accommodate schedules of attorneys and staff. '
          ]
        },
        {
          title: 'Overall Job Knowledge',
          questions: [
            'Understands overall job function and responsibilities.',
            'Familiarity with formats, documents, software and procedures.',
            'Applies new concepts and skills.',
            'Awareness of changes and trends in employee’s profession.'
          ]
        },
        {
          title: 'Communication Skills',
          questions: [
            'Effective verbal or written communication with attorneys, staff, clients, others.',
            'Listens well to instructions.',
            'Avoids repeated reminders from attorneys.',
            'Provides timely status updates.'
          ]
        },
      ]
    },
    {
      title: 'On-The-Job Attitude',
      items: [
        {
          title: 'Initiative',
          questions: [
            'Performs projects without being reminded.',
            'Ability to work with minimal supervision.',
            'Stays abreast of new technology or administrative improvements; develops procedures to assist attorneys and staff in the efficient administration of their work.',
            'Seeks increased assignments and responsibilities; actively identifies and pursues new projects.'
          ]
        },
        {
          title: 'Adaptability',
          questions: [
            'Effectiveness under pressure.',
            'Accepts new methods and changes readily.',
            'Adapts easily to changing priorities.',
            'Modifies schedule to meet work demands.',
            'Response to criticism/suggestions.'
          ]
        },
      ]
    },
    {
      title: 'Personal Effectiveness',
      items: [
        {
          title: 'Interpersonal Skills',
          questions: [
            'Cooperates with attorneys, staff, clients, and others.',
            'Supports team effort and contributes to firm goals.',
            'Is always courteous and acts in a professional manner.'
          ]
        },
        {
          title: 'Organization',
          questions: [
            'Sets and revises priorities as appropriate.',
            'Maintains orderly desk, files and work area.',
            'Dresses appropriately.',
            'Arrives and departs in accordance with firm standards and professional expectations; makes efficient use of time.'
          ]
        },
        {
          title: 'Availability',
          questions: [
            'Maintains appropriate work hours in order to be available to receive feedback from mentor and reviewing attorneys'
          ]
        }
      ]
    }
  ]
}
