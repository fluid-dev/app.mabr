export default {
  groups: [
    {
      items: [
        {
          title: 'Proper Formatting',
          questions: [
            'Does the employee use appropriate documents and shells?  Please comment on the employee’s adoption of proper formatting techniques as they relate to attorney/client preferences.'
          ]
        }
      ]
    },
    {
      items: [
        {
          title: 'Responsiveness',
          questions: [
            'Does the employee complete assigned tasks in accordance with deadlines and provide timely reminders?  Please comment on his/her ability to adapt to changing priorities and deadlines.'
          ]
        }
      ]
    },
    {
      items: [
        {
          title: 'Completeness of Filings',
          questions: [
            'Does the employee include all the necessary documents with filings?  How often are you required to edit filings to ensure conformance with USPTO requirements?  Please cite examples.'
          ]
        }
      ]
    },
    {
      items: [
        {
          title: 'Efficiency',
          questions: [
            'Does the employee reliably track your workflow, monitor deadlines and assist you in remembering and meeting deadlines?  Please evaluate the employee’s productivity and management of workflow and indicate whether due dates or deadlines have been missed.'
          ]
        }
      ]
    },
    {
      items: [
        {
          title: 'Attention to Detail',
          questions: [
            'Does the employee proofread documents and produce accurate work?  How often do you find yourself correcting errors and where are they most prominent?'
          ]
        }
      ]
    },
    {
      items: [
        {
          title: 'Job Knowledge',
          questions: [
            'Does the employee thoroughly understand his/her job function?  Please comment on his/her ability to follow proper procedures and apply learned skills to the job.'
          ]
        }
      ]
    },
    {
      items: [
        {
          title: 'Communication',
          questions: [
            'Does the employee communicate effectively with attorneys, clients, the USPTO and peers?  Does he/she listen to and apply instructions?  Please list any examples of professionalism (or lack thereof) in communicating with others.'
          ]
        }
      ]
    },
    {
      items: [
        {
          title: 'Client Relations',
          questions: [
            'Is the employee responsive to client needs?  Please provide examples of his/her management and application of client preferences and requirements.'
          ]
        }
      ]
    },
    {
      items: [
        {
          title: 'Initiative',
          questions: [
            'Does the employee show initiative in seeking out more challenging assignments?  Please comment on his/her level of resourcefulness and initiative on resolving incoming matters.'
          ]
        }
      ]
    },
    {
      items: [
        {
          title: 'Commitment',
          questions: [
            'Is the employee conscientious and dedicated to the team’s success?  Please comment on the employee’s attendance and commitment to the job.'
          ]
        }
      ]
    }
  ]
}
