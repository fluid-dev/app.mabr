export default {
  groups: [
    {
      title: 'Technical & Professional Skills',
      items: [
        {
          title: 'Quality Of Work',
          questions: [
            'Produces thorough, accurate and consistent work product.',
            'Work product includes proper format, content and style.',
            'Proofreading and accuracy.',
            'Pays attention to details.',
            'Conformity with established procedures.',
          ]
        },
        {
          title: 'Production Level',
          questions: [
            'Consistently completes assignments in a timely manner.',
            'Meets deadlines.',
            'Shoulders share of workload.',
            'Efficient time management skills.'
          ]
        },
        {
          title: 'Job Knowledge',
          questions: [
            'Understands overall job function and responsibilities.',
            'Familiarity with formats, documents, software and procedures.',
            'Applies new concepts and skills.'
          ]
        },
        {
          title: 'Communication Skills',
          questions: [
            'Effective verbal or written communication with supervisor, clients, peers and others.',
            'Listens well to instructions.',
            'Provides timely status updates.'
          ]
        }
      ]
    },
    {
      title: 'On-The-Job Attitude',
      items: [
        {
          title: 'Initiative',
          questions: [
            'Performs tasks without being told.',
            'Ability to work with minimal supervision.',
            'Foresight in managing work details.',
            'Suggests and develops procedures to make tasks easier and results more effective.',
            'Does not remain idle; seeks increased assignments and responsibilities.'
          ]
        },
        {
          title: 'Adaptability',
          questions: [
            'Effectiveness under pressure.',
            'Accepts new methods and changes readily.',
            'Adapts easily to changing priorities.',
            'Modifies schedule to meet work demands.',
            'Response to criticism/suggestions.',
            'Availability for overtime when necessary.'
          ]
        }
      ]
    },
    {
      title: 'Personal Effectiveness',
      items: [
        {
          title: 'Interpersonal Skills',
          questions: [
            'Cooperates with supervisor, peers and others.',
            'Supports team effort and contributes to departmental goals.',
            'Is always courteous and acts in a professional manner.'
          ]
        },
        {
          title: 'Organization',
          questions: [
            'Sets and revises priorities as appropriate.',
            'Maintains orderly desk, files and work area.',
            'Dresses appropriately.',
          ]
        },
        {
          title: 'Attendance & Punctuality',
          questions: [
            'Maintains satisfactory attendance.',
            'Arrives and departs in accordance with firm standards.',
            'Refrains from conducting personal business during working hours.',
            'Takes reasonable breaks and lunch period.'
          ]
        }
      ]
    }
  ]
}
