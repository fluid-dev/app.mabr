// Import VueRouter
import VueRouter from 'vue-router';
import UserGuard from './guards/UserGuard';

// Router configuration
export default new VueRouter({
  linkActiveClass: 'is-active',

  mode: 'history',

  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },

  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: require('./views/Dashboard'),
      meta: {
        auth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: require('./views/auth/Login'),
      meta: {
        auth: false
      }
    },
    {
      path: '/oauth/callback/:provider',
      name: 'oauthLogin',
      component: require('./views/auth/OAuth'),
      props: true,
      meta: {
        auth: false
      }
    },
    {
      path: '/password/reset/:token?',
      name: 'passwordReset',
      props: true,
      component: require('./views/auth/PasswordReset'),
      meta: {
        auth: false
      }
    },
    {
      path: '/requests',
      component: require('./views/matters/AddMatter'),
      name: 'addMatter',
      meta: {
        auth: ['admin', 'marketing', 'user']
      },
    },
    {
      path: '/clients',
      component: require('./views/clients/Clients'),
      meta: {
        auth: true
      },
      children: [
        {
          path: '/',
          name: 'allClients',
          component: require('./views/clients/AllClients'),
        },
        {
          path: 'add',
          name: 'addClient',
          component: require('./views/clients/AddClient'),
          meta: {
            auth: ['admin', 'docket']
          },
        },
        {
          path: ':id',
          name: 'viewClient',
          component: require('./views/clients/ViewClient'),
          props: true
        },
        {
          path: ':id/edit',
          name: 'editClient',
          component: require('./views/clients/EditClient'),
          props: true,
          meta: {
            auth: ['admin', 'docket']
          },
        }
      ]
    },
    {
      path:'/matters',
      component: require('./views/matters/Matters'),
      meta: {
        auth: true
      },
      children: [
        {
          path: '/',
          name: 'allMatters',
          component: require('./views/matters/AllMatters')
        },
        {
          path: ':id',
          name: 'viewMatter',
          component: require('./views/matters/ViewMatter'),
          props: true,
        },
        {
          path: ':id/edit',
          name: 'editMatter',
          component: require('./views/matters/EditMatter'),
          props: true,
          meta: {
            auth: ['admin', 'docket']
          },
        }
      ]
    },
    {
      path: '/people',
      component: require('./views/people/People'),
      meta: {
        auth: true
      },
      children: [
        {
          path: '/',
          name: 'allPeople',
          component: require('./views/people/AllPeople')
        },
        {
          path: 'add',
          name: 'addPerson',
          component: require('./views/people/AddPerson'),
          meta: {
            auth: ['admin', 'hr', 'marketing']
          }
        },
        {
          path: ':id',
          name: 'viewPerson',
          component: require('./views/people/ViewPerson'),
          props: true
        },
        {
          path: ':id/edit',
          name: 'editPerson',
          component: require('./views/people/EditPerson'),
          props: true,
          beforeEnter: UserGuard
        }
      ]
    },
    {
      path: '/evaluations',
      component: require('./views/evaluations/Evaluations'),
      meta: {
        auth: true
      },
      children: [
        {
          path: '/',
          name: 'allEvaluations',
          component: require('./views/evaluations/AllEvaluations'),
          meta: {
            auth: ['admin', 'hr']
          }
        },
        {
          path: 'add',
          name: 'addEvaluation',
          component: require('./views/evaluations/AddEvaluation'),
          meta: {
            auth: ['admin', 'hr']
          }
        },
        {
          path: ':id/self',
          name: 'selfEvaluation',
          component: require('./views/evaluations/SelfEvaluation'),
          props: true
        },
        {
          path: ':id/group',
          name: 'groupEvaluation',
          component: require('./views/evaluations/GroupEvaluation'),
          props: true
        },
        {
          path: ':id',
          name: 'reviewEvaluation',
          component: require('./views/evaluations/ReviewEvaluation'),
          props: true
        },
      ]
    },
    {
      path: '/news',
      component: require('./views/news/Articles'),
      meta: {
        auth: true
      },
      children: [
        {
          path: '/',
          name: 'allArticles',
          component: require('./views/news/AllArticles')
        },
        {
          path: 'add',
          name: 'addArticle',
          component: require('./views/news/AddArticle'),
          meta: {
            auth: ['admin', 'hr', 'marketing']
          }
        },
        {
          path: ':id',
          name: 'viewArticle',
          component: require('./views/news/ViewArticle'),
          props: true
        },
        {
          path: ':id/edit',
          name: 'editArticle',
          component: require('./views/news/EditArticle'),
          props: true,
          meta: {
            auth: ['admin', 'hr', 'marketing']
          }
        }
      ]
    },
    {
      path: '/resources/:resource',
      name: 'allResources',
      component: require('./views/resources/AllResources'),
      props: true,
      meta: {
        auth: true
      }
    },
    {
      path: '/resources/:resource/add',
      name: 'addResource',
      component: require('./views/resources/AddResource'),
      props: true,
      meta: {
        auth: ['admin', 'hr', 'marketing']
      }
    },
    {
      path: '/settings',
      name: 'settings',
      component: require('./views/settings/Settings'),
      meta: {
        auth: ['admin', 'hr', 'marketing']
      },
      children: [
        {
          path: 'general',
          name: 'general',
          component: require('./views/settings/General'),
        },
        {
          path: 'announcements',
          name: 'announcements',
          component: require('./views/settings/Announcements'),
        },
        {
          path: 'shoutouts',
          name: 'shoutOuts',
          component: require('./views/settings/ShoutOuts'),
        },
        ,
        {
          path: 'locations',
          name: 'locations',
          component: require('./views/settings/Locations'),
        },
        {
          path: 'departments',
          name: 'departments',
          component: require('./views/settings/Departments'),
        },
        ,
        {
          path: 'categories',
          name: 'categories',
          component: require('./views/settings/Categories'),
        }
      ]
    },
    {
      path: '/unauthorized',
      name: 'error403',
      component: require('./views/errors/403')
    },
    {
      path: '*',
      name: 'error404',
      component: require('./views/errors/404')
    }
  ]
});
