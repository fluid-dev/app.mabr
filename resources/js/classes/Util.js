export default {
  install: (Vue) => {
    Vue.prototype.$util = {
      clone: function(object) {
        return JSON.parse(JSON.stringify(object));
      }
    }
  }
};
