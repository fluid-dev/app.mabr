import Vue from 'vue';
import SmoothScroll from 'smooth-scroll';

/**
 * Handles server side form validation
 */
export default Vue.extend({
  data() {
    return {
      errorMessage: '',
      errors: {}
    }
  },

  methods: {
    _respondToErrors: function(error) {
      this.$toast.error('Uh oh! This form can\'t be submitted yet.')
      this.$store.commit('setLoading', false)
      this.errors = {};
      let response = error.response.data;

      if (response.hasOwnProperty('errors')) {
        this._scrollTop();
        this.errorMessage = 'There were some errors with your form submission.'
        let fields = Object.keys(response.errors);

        fields.forEach((field, index) => {
          let name = field.replace(/\./g, '_');
          $('[data-abide]').foundation('addErrorClasses', $(`#${name}`));
          this.$set(this.errors, name, response.errors[field][0])
        })
      }
    },

    _scrollTop() {
      let scroll = new SmoothScroll();
      let anchor = document.querySelector('main.view');
      let options = { speed: 750, easing: 'easeInOutQuint' };
      return scroll.animateScroll(anchor, false, options);
    }
  }
});
