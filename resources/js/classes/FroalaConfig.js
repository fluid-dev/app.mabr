let editorButtons = [
  'bold',
  'italic',
  'underline',
  'paragraphFormat',
  'fontSize',
  'insertLink',
  'quote',
  'insertHR',
  'formatOL',
  'formatUL',
  /*'insertImage',*/ '|',
  'undo',
  'redo',
  'selectAll'
];

export default {
  toolbarButtons: editorButtons,
	toolbarButtonsMD: editorButtons,
	toolbarButtonsSM: editorButtons,
	toolbarButtonsXS: editorButtons,
	placeholderText: '',
  theme: 'fr-office-action',
  pluginsEnabled: [
    'fontSize',
    'lists',
    'quote',
    'link',
    'paragraphFormat',
    'fontFamily',
    'image',
    'imageManager',
    'codeView'
  ],
  height: 400,
  theme: 'fr-office-action',
  imageUploadParam: 'image',
	imageUploadURL: '/api/editor',
	imageManagerLoadURL: '/api/editor',
	imageManagerDeleteURL: '/api/editor',
	imageManagerDeleteMethod: 'delete'
}
