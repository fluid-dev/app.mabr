/**
 * Converts an object of arrays into a comma separated string
 *
 * @param  object
 * @return string
 */
export default function commaSeparated(items, key) {
  let list = [];

  items.forEach((item, i) => list.push(item[key]));

  return list.join(', ');
}
