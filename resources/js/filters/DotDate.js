import moment from 'moment';

/**
 * Converts a timestamp to MM.DD.YYYY format
 *
 * @param  string date Timestamp
 * @return string moment()
 */
export default function dotDate(date) {
  return moment(date).format('MM.DD.YYYY');
}
