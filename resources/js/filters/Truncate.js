/**
 * Truncates a string to 50 characters
 *
 * @param  string
 * @return string
 */
export default function truncate(string) {
  let excerpt = string.substring(0, 75);

  if (string.length > 75) {
    excerpt += '...';
  }

  return excerpt;
}
