import moment from 'moment';

/**
 * Converts a timestamp to MM.DD.YYYY format
 *
 * @param  string date Timestamp
 * @return string moment()
 */
export default function slashDate(date, showYear = true) {
  let format = (showYear) ? 'MMMM Do, YYYY' : 'MMMM Do';
  return moment(date).format(format);
}
