<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationContributorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluation_contributors', function (Blueprint $table) {
            $table->unsignedInteger('evaluation_id');
            $table->foreign('evaluation_id')
                ->references('id')->on('evaluations')
                ->onDelete('cascade');
            $table->unsignedInteger('person_id');
            $table->foreign('person_id')
                ->references('id')->on('people')
                ->onDelete('cascade');
            $table->dateTime('completed')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluation_contributors');
    }
}
