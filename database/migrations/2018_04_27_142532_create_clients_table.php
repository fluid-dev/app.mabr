<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('type')->nullable();
            $table->foreign('type')
                ->references('id')->on('client_types');
            $table->string('name');
            $table->enum('status', ['pending', 'docketing', 'exporting', 'completed']);
            $table->string('number')->nullable()->unique();
            $table->string('dba')->nullable();
            $table->string('contact_name')->nullable();
            $table->string('street_address');
            $table->string('city');
            $table->string('state');
            $table->string('zip');
            $table->string('country');
            $table->string('contact_phone')->nullable();
            $table->string('billing_phone')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('billing_email')->nullable();
            $table->unsignedInteger('originating_attorney')->nullable();
            $table->foreign('originating_attorney')
                ->references('id')->on('people')
                ->onDelete('set null');
            $table->unsignedInteger('billing_attorney')->nullable();
            $table->foreign('billing_attorney')
                ->references('id')->on('people')
                ->onDelete('set null');
            $table->unsignedInteger('working_attorney')->nullable();
            $table->foreign('working_attorney')
                ->references('id')->on('people')
                ->onDelete('set null');
            $table->text('details')->nullable();
            $table->string('image')->nullable();
            $table->boolean('anaqua')->default(false);
            $table->boolean('imanage')->default(false);
            $table->timestamps();
        });

        // DB::statement('ALTER TABLE clients AUTO_INCREMENT = 14034');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients', function ($table) {
            $table->dropForeign('originating_attorney');
            $table->dropForeign('billing_attorney');
            $table->dropForeign('working_attorney');
        });
    }
}
