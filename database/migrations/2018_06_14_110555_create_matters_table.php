<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMattersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_id');
            $table->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('cascade');
            $table->string('type_name');
            $table->integer('type_class');
            $table->string('title');
            $table->unsignedInteger('responsible_attorney');
            $table->foreign('responsible_attorney')
                ->references('id')
                ->on('people')
                ->onDelete('cascade');
            $table->unsignedInteger('working_attorney');
            $table->foreign('working_attorney')
                ->references('id')
                ->on('people')
                ->onDelete('cascade');
            $table->text('remarks')->nullable();
            $table->text('inventors')->nullable();
            $table->string('reference_number')->nullable();
            $table->string('patent_country')->nullable();
            $table->string('patent_type')->nullable();
            $table->string('patent_application_type')->nullable();
            $table->datetime('target_filing_date')->nullable();
            $table->string('mark')->nullable();
            $table->string('class')->nullable();
            $table->string('mark_country')->nullable();
            $table->text('narrative_summary')->nullable();
            $table->string('adverse_party')->nullable();
            $table->string('representation')->nullable();
            $table->text('keywords')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matters');
    }
}
