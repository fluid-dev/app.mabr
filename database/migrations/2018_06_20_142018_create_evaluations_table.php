<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('person_id');
            $table->foreign('person_id')
                ->references('id')->on('people')
                ->onDelete('cascade');
            $table->dateTime('period_start');
            $table->dateTime('period_end');
            $table->string('type');
            $table->text('self_eval')->nullable();
            $table->text('review_comments')->nullable();
            $table->dateTime('group_eval')->nullable();
            $table->dateTime('approved')->nullable();
            $table->dateTime('acknowledged')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluations');
    }
}
