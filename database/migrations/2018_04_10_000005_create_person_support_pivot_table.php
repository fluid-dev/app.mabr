<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonSupportPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person_support', function (Blueprint $table) {
            $table->unsignedInteger('supporting_id')
                ->index();
            $table->foreign('supporting_id')
                ->references('id')
                ->on('people')
                ->onDelete('cascade');
            $table->unsignedInteger('supported_id')
                ->index();
            $table->foreign('supported_id')
                ->references('id')
                ->on('people')
                ->onDelete('cascade');
            $table->primary(['supporting_id', 'supported_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('person_support');
    }
}
