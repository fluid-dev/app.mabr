<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('title');
            $table->dateTime('hire_date')->nullable();
            $table->dateTime('birthday')->nullable();
            $table->unsignedInteger('role_id')->nullable();
            $table->foreign('role_id')
                ->references('id')->on('roles')
                ->onDelete('set null');
            $table->unsignedInteger('department_id')->nullable();
            $table->foreign('department_id')
                ->references('id')->on('departments')
                ->onDelete('set null');
            $table->unsignedInteger('location_id')->nullable();
            $table->foreign('location_id')
                ->references('id')->on('locations')
                ->onDelete('set null');
            $table->string('cell_phone')->nullable();
            $table->string('direct_phone')->nullable();
            $table->string('image')->nullable();
            $table->text('quick_links')->nullable();
            $table->string('address')->nullable();
            $table->boolean('public_address')->default(false);
            $table->text('bar_numbers')->nullable();
            $table->string('emergency_contact')->nullable();
            $table->string('emergency_phone')->nullable();
            $table->string('racial_identity')->nullable();
            $table->string('gender_identity')->nullable();
            $table->string('sexual_orientation')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people', function ($table) {
            $table->dropForeign('role_id');
            $table->dropForeign('department_id');
            $table->dropForeign('location_id');
        });
    }
}
