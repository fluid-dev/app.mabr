<?php

use Illuminate\Database\Seeder;
use App\Location;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locations = [
            [
                'name' => 'Park City Office',
                'address' => '1389 Center Dr.',
                'building' => 'Suite 300',
                'city' => 'Park City',
                'state' => 'UT',
                'zip' => '84098',
                'phone' => '(435) 252-1360',
            ],
            [
                'name' => 'Salt Lake City Office',
                'address' => '111 S. Main Street',
                'building' => 'Suite 600',
                'city' => 'Salt Lake City',
                'state' => 'UT',
                'zip' => '84111',
                'phone' => '(801) 297-1850',
            ],
            [
                'name' => 'Irvine Office',
                'address' => '20 Pacifica',
                'building' => 'Suite 1130',
                'city' => 'Irvine',
                'state' => 'CA',
                'zip' => '92618',
                'phone' => '(949) 202-1900',
            ],
        ];

        foreach ($locations as $location) {
            $add = Location::create($location);
        }
    }
}
