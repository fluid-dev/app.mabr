<?php

use Illuminate\Database\Seeder;
use App\Department;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Attorneys', 'slug' => 'attorneys'],
            ['name' => 'Administration', 'slug' => 'administration']
        ];

        foreach ($items as $item) {
            $add = Department::create($item);
        }
    }
}
