<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['name' => 'Admin', 'slug' => 'admin'],
            ['name' => 'HR', 'slug' => 'hr'],
            ['name' => 'Marketing', 'slug' => 'marketing'],
            ['name' => 'User', 'slug' => 'user'],
            ['name' => 'Docket', 'slug' => 'docket']
        ];

        foreach ($roles as $role) {
            $add = Role::create($role);
        }
    }
}
