<?php

use App\ClientType;
use Illuminate\Database\Seeder;

class ClientTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Potential Client', 'slug' => 'potential-client'],
            ['name' => 'Current Client', 'slug' => 'current-client'],
            ['name' => 'Former Client', 'slug' => 'former-client'],
            ['name' => 'Firm Contact', 'slug' => 'firm-contact']
        ];

        foreach ($items as $item) {
            $add = ClientType::create($item);
        }
    }
}
