<?php

use App\Setting;

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $site_links = Setting::create([
            'key'   => 'site_links',
            'value' => [
                [
                    'name' => 'Google',
                    'link' => 'google.com'
                ]
            ]
        ]);

        $faqs = Setting::create([
            'key'   => 'faqs',
            'value' => [
                [
                    'question' => 'What color is the sky?',
                    'answer' => 'It\'s BLUE, ya dingus!'
                ]
            ]
        ]);

        $help_menu = Setting::create([
            'key'   => 'help_menu',
            'value' => 'For any questions or technical assistance, please contact the help desk at <a href="tel:888.888.8888">888.888.8888</a> or email them at <a href="mailto:help@website.com?">help@website.com</a>'
        ]);

        $notifications = Setting::create([
            'key' => 'notifications',
            'value' => [
                'anaqua_export_email' => 'something@mabr.com',
                'evaluation_email' => 'something@mabr.com',
                'docketing_email' => 'something@mabr.com'
            ]
        ]);
    }
}
