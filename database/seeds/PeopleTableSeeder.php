<?php

use App\Person;

use Carbon\Carbon;

use Illuminate\Database\Seeder;

class PeopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user                = new Person();
        $user->first_name    = 'John';
        $user->last_name     = 'Doe';
        $user->email         = 'john@example.com';
        $user->password      = bcrypt('secret');
        $user->title         = 'Admin';
        $user->hire_date     = Carbon::now()->subYear(100);
        $user->birthday      = Carbon::now()->subYear(100);
        $user->role_id       = 1;
        $user->department_id = 1;
        $user->location_id   = 1;
        $user->cell_phone    = '(801) 867-5309';
        $user->direct_phone  = '(801) 555-1234';

        $user->save();
    }
}
