<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Person::class, function (Faker $faker) {
    return [
        'first_name'    => $faker->firstName,
        'last_name'     => $faker->lastName,
        'email'         => $faker->unique()->safeEmail,
        'password'      => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'title'         => $faker->jobTitle(),
        'hire_date'     => $faker->dateTimeBetween('-25 years', '-5 years')->format('Y-m-d H:i:s'),
        'birthday'      => $faker->dateTime('-20 years')->format('Y-m-d H:i:s'),
        'role_id'       => $faker->numberBetween(1, 8),
        'department_id' => $faker->numberBetween(1, 2),
        'location_id'   => $faker->numberBetween(1, 3),
        'cell_phone'  => $faker->tollFreePhoneNumber(),
        'direct_phone'  => $faker->tollFreePhoneNumber()
    ];
});
