<?php

use App\Category;

use Faker\Generator as Faker;

$factory->define(App\Article::class, function (Faker $faker) {
    return [
        'title'        => ucwords($faker->words(3, true)),
        'content'      => $faker->realText(3000),
        'category_id'  => Category::where('type', 'article')->inRandomOrder()->first()->id,
        'author_id'    => $faker->numberBetween(1, 400),
        'published_at' => $faker->dateTimeThisYear()->format('Y-m-d H:i:s')
    ];
});
