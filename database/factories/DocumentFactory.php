<?php

use App\Category;

use Faker\Generator as Faker;

$factory->define(App\Resource::class, function (Faker $faker) {
    $date = $faker->dateTimeThisYear()->format('Y-m-d H:i:s');
    $type = array_random(['form', 'marketing-collateral']);

    return [
        'name'        => $faker->sentence(3),
        'type'        => $type,
        'category_id' => Category::where('type', $type)->inRandomOrder()->first()->id,
        'path'        => 'pdf-sample.pdf',
        'created_at'  => $date,
        'updated_at'  => $date
    ];
});
