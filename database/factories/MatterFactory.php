<?php

use Faker\Generator as Faker;

$factory->define(App\Matter::class, function (Faker $faker) {
    return [
        'client_id'               => $faker->numberBetween(1, 1000),
        'type'                    => $faker->sentence,
        'title'                   => $faker->sentence,
        'remarks'                 => $faker->paragraph,
        'inventors'               => $faker->sentence,
        'reference_number'        => $faker->swiftBicNumber,
        'patent_country'          => $faker->country,
        'patent_type'             => $faker->word,
        'patent_application_type' => $faker->word,
        'target_filing_date'      => $faker->dateTimeBetween('-30 years', '-1 month')->format('Y-m-d H:i:s'),
        'mark'                    => $faker->sentence,
        'class'                   => $faker->sentence,
        'mark_country'            => $faker->sentence,
        'narrative_summary'       => $faker->paragraph,
        'adverse_party'           => $faker->sentence,
        'representation'          => $faker->sentence,
        'keywords'                => $faker->paragraph
    ];
});
