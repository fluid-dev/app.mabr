<?php

use Faker\Generator as Faker;

$factory->define(App\Client::class, function (Faker $faker) {
    return [
        'type'                 => $faker->numberBetween(1, 4),
        'name'                 => $faker->company(),
        'status'               => 'completed',
        'number'               => $faker->unique()->postcode,
        'dba'                  => $faker->company(),
        'contact_name'         => $faker->name(),
        'street_address'       => $faker->streetAddress(),
        'city'                 => $faker->city(),
        'state'                => $faker->stateAbbr(),
        'zip'                  => $faker->postcode(),
        'contact_phone'        => $faker->tollFreePhoneNumber(),
        'billing_phone'        => $faker->tollFreePhoneNumber(),
        'contact_email'        => $faker->email(),
        'billing_email'        => $faker->email(),
        'originating_attorney' => $faker->numberBetween(1, 400),
        'billing_attorney'     => $faker->numberBetween(1, 400),
        'working_attorney'     => $faker->numberBetween(1, 400),
        'details'              => $faker->paragraph()
    ];
});
