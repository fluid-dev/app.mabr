<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'                 => 'required|exists:client_types,id',
            'name'                 => 'required|string|max:255',
            'dba'                  => 'nullable|string|max:255',
            'originating_attorney' => 'required|exists:people,id',
            'billing_attorney'     => 'exists:people,id',
            'working_attorney'     => 'exists:people,id',
            'street_address'       => 'required|string|max:255',
            'city'                 => 'required|string|max:255',
            'state'                => 'required|string|max:2',
            'zip'                  => 'required|string',
            'country'              => 'required|string',
            'contact_name'         => 'string|max:255',
            // 'contact_phone'        => 'regex:/^[(]\d{3}[)] \d{3}-\d{4}$/',
            'contact_email'        => 'string|email|max:255',
            // 'billing_phone'        => 'regex:/^[(]\d{3}[)] \d{3}-\d{4}$/',
            'billing_email'        => 'nullable|string|email|max:255'
        ];
    }
}
