<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EvaluationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasRole(['admin', 'marketing', 'hr']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'person_id'      => 'required|exists:people,id',
            'type'           => 'required|integer',
            'contributors.*' => 'required|exists:people,id',
            'period_start'   => 'required',
            'period_end'     => 'required'
        ];
    }
}
