<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MatterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id'            => 'required|exists:clients,id',
            'type_name'            => 'required|string|max:255',
            'type_class'           => 'required|integer',
            'title'                => 'required|string|max:255',
            'responsible_attorney' => 'required',
            'working_attorney'     => 'required'
        ];
    }
}
