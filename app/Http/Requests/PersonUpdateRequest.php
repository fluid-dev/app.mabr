<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

use Illuminate\Foundation\Http\FormRequest;

class PersonUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasRole(['admin', 'marketing', 'hr']) || auth()->id() === request()->id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'         => 'required|string|max:255',
            'last_name'          => 'required|string|max:255',
            'email'              => 'required|string|email|max:255|unique:people',
            'title'              => 'required|string|max:255',
            // 'emergency_phone'    => 'nullable|regex:/^[(]\d{3}[)] \d{3}-\d{4}$/',
            'role_id'            => 'required|exists:roles,id',
            'department_id'      => 'required|exists:departments,id',
            // 'location_id'        => 'required|exists:locations,id',
            // 'direct_phone'       => 'nullable|regex:/^[(]\d{3}[)] \d{3}-\d{4}$/',
            // 'office_phone'       => 'nullable|regex:/^[(]\d{3}[)] \d{3}-\d{4}$/',
            'password'           => 'confirmed',
            'quick_links.*.name' => 'required',
            'quick_links.*.link' => 'required',
            'email'              => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique('people')->ignore(request()->id),
            ]
        ];
    }
}
