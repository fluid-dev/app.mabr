<?php

namespace App\Http\Controllers;

use App\Client;
use League\Csv\Writer;

class ClientProcessController extends Controller
{
    public function approve(Client $client)
    {
        $client->status = 'docketing';

        return $this->clientDecision($client, 2);
    }

    public function reject(Client $client)
    {
        $client->status = 'completed';

        return $this->clientDecision($client, 4);
    }

    public function clientDecision(Client $client, $type)
    {
        $client->type = $type;

        $client->save();

        $client->load('originatingAttorney', 'workingAttorney', 'billingAttorney', 'matters', 'type');

        return response()->json($client, 200);
    }

    public function anaquaCsv(Client $client)
    {
        $client->anaqua = true;
        $client->save();

        $header = ['name', 'contact_name', 'dba', 'contact_email', 'billing_email'];

        $csv = Writer::createFromFileObject(new \SplTempFileObject());

        $csv->insertOne($header);
        $csv->insertOne([
            $client->name,
            $client->contact_name,
            $client->dba,
            $client->contact_email,
            $client->billing_email
        ]);

        $csv->output('anaqua.csv');
    }

    public function iManageCsv(Client $client)
    {
        $client->imanage = true;
        $client->save();

        $header = ['name', 'contact_name', 'dba', 'contact_email', 'billing_email'];

        $csv = Writer::createFromFileObject(new \SplTempFileObject());

        $csv->insertOne($header);
        $csv->insertOne([
            $client->name,
            $client->contact_name,
            $client->dba,
            $client->contact_email,
            $client->billing_email
        ]);

        $csv->output('imanage.csv');
    }
}
