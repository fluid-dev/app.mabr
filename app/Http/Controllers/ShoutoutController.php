<?php

namespace App\Http\Controllers;

use App\Shoutout;
use Illuminate\Http\Request;

class ShoutoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shoutouts = Shoutout::latest()->with('person')->paginate(25);

        return response($shoutouts, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'message'   => 'required|string|max:200',
            'anonymous' => 'required',
        ]);

        $shoutout = new Shoutout;

        $shoutout->message   = $request->message;
        $shoutout->anonymous = $request->anonymous;
        $shoutout->person_id = auth()->id();

        $shoutout->save();

        return response()->json($shoutout, 200);
    }

    /**
     * Approve the specified shoutout.
     *
     * @param  \App\Shoutout  $shoutout
     * @return \Illuminate\Http\Response
     */
    public function approve(Shoutout $shoutout)
    {
        $shoutout->approved = now();
        $shoutout->save();

        return response()->json($shoutout, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shoutout  $shoutout
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shoutout $shoutout)
    {
        $shoutout->delete();

        return response()->json($shoutout, 200);
    }
}
