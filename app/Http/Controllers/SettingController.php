<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $_settings = Setting::all();
        $settings = [];

        foreach ($_settings as $setting) {
            $settings[$setting->key] = $setting->value;
        }

        return response()->json([
            'settings' => $settings
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $setting = Setting::updateOrCreate([
            'key' => $request->key
        ], $request->all());

        return response()->json($setting, 200);
    }
}
