<?php

namespace App\Http\Controllers\Auth;

use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\Controller;

class AzureLoginController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('azure')->stateless()->redirect();
    }
}
