<?php

namespace App\Http\Controllers\Auth;

use App\Shoutout;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use App\Person;

class LoginController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', [
            'except' => [
                'login',
                'redirectToProvider',
                'handleProviderCallback'
            ]
        ]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['msg' => 'Hmm... we didn\'t find a user with those credentials. Try again!'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get a JWT via Azure User object.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function handleProviderCallback()
    {
        $azure = Socialite::driver('azure')->stateless()->user();

        $name = explode(' ', $azure->getName());

        // First or Create a user
        $person = Person::firstOrCreate([
            'email' => $azure->getEmail()
        ], [
            'email' => $azure->getEmail(),
            'first_name' => $name[0],
            'last_name' => $name[1],
            'title' => 'Azure User',
            'role_id' => 4
        ]);

        if (!$token = auth()->login($person)) {
            return response()->json(['msg' => 'Hmm... we didn\'t find a user with those credentials. Try again!'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function user()
    {
        $user = auth()
            ->user()
            ->load([
                'department',
                'location',
                'selfEvaluations',
                'employeeEvaluations',
                // 'evaluationReviews',
                'completedEvaluations'
            ]);

        if ($user->hasRole('marketing') || $user->hasRole('admin')) {
            $user->approvable_shoutouts = Shoutout::where('approved', null)->count();
        }

        $user->roles = [$user->role->slug];

        return response()->json($user);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response(null, 201)
            ->header('Authorization', $token);
    }
}
