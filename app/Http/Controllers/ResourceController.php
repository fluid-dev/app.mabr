<?php

namespace App\Http\Controllers;

use App\Resource;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Department;

class ResourceController extends Controller
{
    /**
     * Resource storage location
     *
     * @var String
     */
    protected $storage;
    protected $type;
    protected $sort;

    public function __construct(Request $request)
    {
        $this->type = $request->type;
        $this->sort = $request->sort;
        $this->storage = 'public/resources/'.$this->type;
    }

    /**
     * Display a listing of the resources.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $resources = Resource::type($this->type)
            ->orderBy($request->order_by, $request->order)
            ->when($request->has('category'), function ($query) use ($request) {
                return $query->where('category_id', $request->category);
            })
            ->when($request->has('search'), function ($query) use ($request) {
                return $query->where('name', 'LIKE', "%{$request->search}%");
            })
            ->paginate(25);

        $categories = Category::for($request->type);

        $department = Department::where('slug', $this->type)->firstOrFail();

        return response()->json([
            'resources'  => $resources,
            'categories' => $categories,
            'department' => $department
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $categories = Category::for($this->type);
        $department = Department::where('slug', $this->type)->firstOrFail();

        return response()->json([
            'categories' => $categories,
            'department' => $department
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if (!auth()->user()->hasRole('admin')) {
            return response('Unauthorized', 403);
        }

        $this->validate($request, [
            'name'        => 'required|string|max:255',
            'type'        => 'required',
            'path'        => 'required',
        ]);

        $resource         = new Resource();
        $resource->name   = $request->name;
        $resource->type   = $request->type;
        $resource->source = $request->source;

        if ($request->source == 'file') {
            $path = $request->file('path')->store($this->storage);
            $resource->path = str_replace("{$this->storage}/", '', $path);
        }

        if ($request->source == 'url') {
            $resource->path = $request->path;
        }

        if ($request->has('add_category')) {
            $resource->category_id = Category::add($request->category_id, $this->type);
        } else {
            $resource->category_id = $request->category_id;
        }

        $resource->save();

        return response()->json($resource, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Resource  $resource
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Resource $resource)
    {
        $resource->delete();

        return response()->json($resource, 200);
    }
}
