<?php

namespace App\Http\Controllers;

use App\Client;
use App\Matter;
use App\Person;
use App\Article;
use App\Resource;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    private $results_limit = 3;

    public function results(Request $request)
    {
        $results = [];

        $clients = Client::where('name', 'LIKE', '%'.$request->search.'%')
            ->take($this->results_limit)
            ->get();

        if (count($clients)) {
            $results[] = [
                'title' => 'Clients',
                'route' => 'viewClient',
                'results' => $clients,
                'property' => 'name'
            ];
        }

        $matters = Matter::where('title', 'LIKE', '%'.$request->search.'%')
            ->take($this->results_limit)
            ->get();

        if (count($matters)) {
            $results[] = [
                'title' => 'Matters',
                'route' => 'viewMatter',
                'results' => $matters,
                'property' => 'title'
            ];
        }

        $people = Person::where('first_name', 'LIKE', '%'.$request->search.'%')
            ->orWhere('last_name', 'LIKE', '%'.$request->search.'%')
            ->take($this->results_limit)
            ->get();

        if (count($people)) {
            $results[] = [
                'title' => 'People',
                'route' => 'viewPerson',
                'results' => $people,
                'property' => 'full_name'
            ];
        }

        $resources = Resource::where('name', 'LIKE', '%'.$request->search.'%')
            ->take($this->results_limit)
            ->get();

        if (count($resources)) {
            $results[] = [
                'title' => 'Resources',
                'route' => null,
                'results' => $resources,
                'property' => 'name'
            ];
        }

        $articles = Article::where('title', 'LIKE', '%'.$request->search.'%')
            ->take($this->results_limit)
            ->get();

        if (count($articles)) {
            $results[] = [
                'title' => 'Articles',
                'route' => 'viewArticle',
                'results' => $articles,
                'property' => 'title'
            ];
        }

        return response()->json($results, 200);
    }
}
