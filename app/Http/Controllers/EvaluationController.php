<?php

namespace App\Http\Controllers;

use App\Person;
use App\Evaluation;
use League\Csv\Writer;
use App\EvaluationResponse;
use Illuminate\Http\Request;
use App\Http\Requests\EvaluationRequest;
use App\Http\Requests\SelfEvaluationRequest;
use App\Http\Requests\GroupEvaluationRequest;
use App\Events\Evaluations\EvaluationCreated;
use App\Events\Evaluations\EvaluationApproved;
use App\Events\Evaluations\SelfEvaluationCompleted;
use App\Events\Evaluations\GroupEvaluationCompleted;

class EvaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $evaluations = Evaluation::latest()
            ->when($request->has('type'), function ($query) use ($request) {
                return $query
                    ->where('type', $request->type);
            })
            ->when($request->has('search'), function ($query) use ($request) {
                return $query
                    ->whereHas('employee', function ($query) use ($request) {
                        return $query
                            ->where('first_name', 'LIKE', "%{$request->search}%")
                            ->orWhere('last_name', 'LIKE', "%{$request->search}%");
                    })
                    ->orWhereHas('contributors', function ($query) use ($request) {
                        return $query
                            ->where('first_name', 'LIKE', "%{$request->search}%")
                            ->orWhere('last_name', 'LIKE', "%{$request->search}%");
                    });
            })
            ->paginate(25);

        return response()->json($evaluations, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $people = Person::orderBy('last_name')->get();

        return response()->json([
            'people' => $people
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\EvaluationRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EvaluationRequest $request)
    {
        $evaluation               = new Evaluation;
        $evaluation->person_id    = $request->person_id;
        $evaluation->period_start = $request->period_start;
        $evaluation->period_end   = $request->period_end;
        $evaluation->type         = $request->type;

        $evaluation->save();

        $evaluation->contributors()->attach($request->contributors);

        event(new EvaluationCreated($evaluation));

        return $this->show($evaluation);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function show(Evaluation $evaluation)
    {
        return response()->json($evaluation, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function edit(Evaluation $evaluation)
    {
        return $this->show($evaluation);
    }

    /**
     * Update the model with the Self Evaluation details
     *
     * @param \App\Http\Requests\SelfEvaluationRequest $request
     * @param \App\Evaluation $evaluation
     * @return \Illuminate\Http\Response
     */
    public function selfEvaluation(SelfEvaluationRequest $request, Evaluation $evaluation)
    {
        $evaluation->self_eval = $request->self_eval;

        $evaluation->update();

        event(new SelfEvaluationCompleted($evaluation));

        return $this->show($evaluation);
    }

    /**
     * Update the model with a Group Evaluation details
     *
     * @param \App\Http\Requests\GroupEvaluationRequest $request
     * @param \App\Evaluation $evaluation
     * @return \Illuminate\Http\Response
     */
    public function groupEvaluation(GroupEvaluationRequest $request, Evaluation $evaluation)
    {
        foreach ($request->questionnaire as $key => $value) {
            $input = [
                'evaluation_id' => $evaluation->id,
                'evaluator_id'  => auth()->id(),
                'response_id'   => $key,
                'response'      => $value
            ];

            $response = EvaluationResponse::updateOrCreate([
                'response_id'   => $key,
                'evaluation_id' => $evaluation->id,
                'evaluator_id'  => auth()->id()
            ], $input);
        }

        $evaluation->contributors()->updateExistingPivot(auth()->id(), [
            'completed' => now()
        ]);

        event(new GroupEvaluationCompleted($evaluation));

        return $this->show($evaluation->fresh());
    }

    /**
     * Approve an Evaluation
     *
     * @param \App\Http\Requests\GroupEvaluationRequest $request
     * @param \App\Evaluation $evaluation
     * @return \Illuminate\Http\Response
     */
    public function approveEvaluation(Evaluation $evaluation)
    {
        $evaluation->approved = now();

        $evaluation->save();

        event(new EvaluationApproved($evaluation));

        return $this->show($evaluation);
    }

    public function exportEvaluation(Evaluation $evaluation)
    {
        $this->approveEvaluation($evaluation);

        $header = ['Employee', 'Evaluators', 'Type', 'Period', 'Average Score'];

        $csv = Writer::createFromFileObject(new \SplTempFileObject());

        $evaluator_names = [];

        foreach ($evaluation->contributors as $contributor) {
            $evaluator_names[] = $contributor->full_name;
        }

        $evaluators = implode(', ', $evaluator_names);

        $evaluation_type = '';

        switch ($evaluation->type) {
            case 1:
                $evaluation_type = 'Adminstrative';
                break;

            case 2:
                $evaluation_type = 'Litigation';
                break;

            case 3:
                $evaluation_type = 'Prosecution';
                break;

            default:
                $evaluation_type = 'Adminstrative';
                break;
        }

        $csv->insertOne($header);
        $csv->insertOne([
            $evaluation->employee->full_name,
            $evaluators,
            $evaluation_type,
            "{$evaluation->period_start->format('m/d/Y')} - {$evaluation->period_end->format('m/d/Y')}",
            $evaluation->average_score
        ]);

        $csv->output("evaluation-{$evaluation->id}.csv");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function update(Evaluation $evaluation, Request $request)
    {
        if (!$evaluation->employee->is_auth) {
            return response('Not Authorized to approve this evaluation', 403);
        }

        $evaluation->acknowledged = now();

        if ($request->filled('review_comments')) {
            $evaluation->review_comments = $request->review_comments;
        }

        $evaluation->save();

        return $this->show($evaluation);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Evaluation $evaluation)
    {
        $evaluation->delete();

        return $this->show($evaluation);
    }
}
