<?php

namespace App\Http\Controllers;

use App\Matter;
use App\Person;
use App\Client;
use Illuminate\Http\Request;
use App\Http\Requests\MatterRequest;
use App\Mail\ConflictCheck;
use App\Setting;
use Illuminate\Support\Facades\Mail;

class MatterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $matters = Matter::query()
            ->when($request->filled('order_by'), function ($query) use ($request) {
                return $query->orderBy($request->order_by, $request->order);
            })
            ->when($request->has('search'), function ($query) use ($request) {
                return $query
                    ->where('type_name', 'LIKE', "%{$request->search}%")
                    ->orWhere('title', 'LIKE', "%{$request->search}%")
                    ->orWhereHas('client', function ($query) use ($request) {
                        return $query->where('name', 'LIKE', "%{$request->search}%");
                    });
            })
            ->latest()
            ->paginate(25);

        return response()->json($matters, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = getCountries();
        $attorneys = Person::orderBy('last_name')->get();
        $matter_types = matterTypes();
        $clients = Client::where('type', 2)
            ->where('number', '!=', null)
            ->orderBy('name')
            ->get();

        return response()->json([
            'clients' => $clients,
            'attorneys' => $attorneys,
            'countries' => $countries,
            'matter_types' => $matter_types
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\MatterRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MatterRequest $request)
    {
        $matter = Matter::create($request->all());

        $setting = Setting::byKey('notifications');

        Mail::to($setting['docketing_email'])->send(new ConflictCheck($matter));

        return response()->json($matter, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Matter  $matter
     * @return \Illuminate\Http\Response
     */
    public function show(Matter $matter)
    {
        return response()->json($matter, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Matter  $matter
     * @return \Illuminate\Http\Response
     */
    public function edit(Matter $matter)
    {
        return $this->show($matter);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\MatterRequest  $request
     * @param  \App\Matter  $matter
     * @return \Illuminate\Http\Response
     */
    public function update(MatterRequest $request, Matter $matter)
    {
        $matter->update($request->all());

        return response()->json($request, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Matter  $matter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Matter $matter)
    {
        $matter->delete();

        return response()->json($matter, 200);
    }
}
