<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EditorController extends Controller
{
    /**
     * Document storage location
     *
     * @var String
     */
    protected $storage = 'public/editor';

    /**
     * Display a listing of the editor image.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Storage::allFiles('/public/editor');

        $items = [];

        foreach ($images as $image) {
            $items[] = [
                'url' => url(str_replace("{$this->storage}/", 'editor/', $image))
            ];
        }

        return response()->json($items, 200);
    }

    /**
     * Store a newly created editor image in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('image')->store($this->storage);

        return response()->json([
            'link' => url(str_replace("{$this->storage}/", 'editor/', $image))
        ], 200);
    }

    /**
     * Remove the specified editor image from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $image = parse_url($request->src, PHP_URL_PATH);
        Storage::disk('public')
            ->delete($image);

        return response()->json([
            'message' => 'image deleted'
        ], 200);
    }
}
