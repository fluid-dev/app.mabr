<?php

namespace App\Http\Controllers;

use App\Article;
use App\Setting;
use App\Location;
use App\Shoutout;
use App\Announcement;
use App\Person;
use App\Client;
use App\Matter;
use App\Department;
use Spatie\GoogleCalendar\Event;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class AppController extends Controller
{
    public function dashboard()
    {
        $articles = Article::with('category')
            ->latest()
            ->take(3)
            ->get();

        $shoutouts = Shoutout::with('person')
            ->latest()
            ->recent()
            ->approved()
            ->get();

        $announcements = Announcement::with('person')
            ->latest()
            ->recent()
            ->get();

        $total_attorneys = Person::where('department_id', 1)->count();
        $total_employees = Person::all()->count();
        $total_clients   = Client::all()->count();
        $total_matters   = Matter::all()->count();

        $locations = Location::all();

        $site_links = Setting::byKey('site_links');

        $calendar = $this->getCalendarEvents();

        return response()->json([
            'articles'      => $articles,
            'shoutouts'     => $shoutouts,
            'announcements' => $announcements,
            'locations'     => $locations,
            'site_links'    => $site_links,
            'calendar'      => $calendar,
            'statistics'    => [
                'attorneys' => $total_attorneys,
                'employees' => $total_employees,
                'clients'   => $total_clients,
                'matters'   => $total_matters
            ]
        ], 200);
    }

    public function navbar()
    {
        $departments = Department::orderBy('name')->get();

        return response()->json([
            'departments' => $departments
        ], 200);
    }

    public function getCalendarEvents()
    {
        $events = [];

        if (Cache::has('google_calendar_response')) {
            $events = Cache::get('google_calendar_response');
        } else {
            $date = new \DateTime();

            $parse = $date->format('Y') . $date->format('m') . '01';

            $mb_events     = Event::get(Carbon::parse($parse), Carbon::parse($parse)->addYears(2), [], 'mabrevents@gmail.com');
            $birthdays     = Event::get(Carbon::parse($parse), Carbon::parse($parse)->addYears(2), [], 'k0gg3r77n1urj47ag79ton1l2k@group.calendar.google.com');
            $firm_events   = Event::get(Carbon::parse($parse), Carbon::parse($parse)->addYears(2), [], '84qus6jgi042s7qhnbjarfi2l0@group.calendar.google.com');
            $firm_holidays = Event::get(Carbon::parse($parse), Carbon::parse($parse)->addYears(2), [], '2b73q7emkthka3n15hmvv9kqfk@group.calendar.google.com');
            $marketing     = Event::get(Carbon::parse($parse), Carbon::parse($parse)->addYears(2), [], 'lsepkk8f84usnbinmfu6f672g0@group.calendar.google.com');

            $events = $mb_events->merge($birthdays)->merge($firm_events)->merge($firm_holidays)->merge($marketing);

            Cache::put('google_calendar_response', $events, now()->addMinutes(60));
        }

        $calendar = [];

        foreach ($events as $event) {
            $calendar[] = [
                'title' => $event->summary,
                'start' => $event->start->date ?: $event->start->dateTime,
                'end'   => $event->end->date ?: $event->end->dateTime
            ];
        }

        return $calendar;
    }
}
