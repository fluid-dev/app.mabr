<?php

namespace App\Http\Controllers;

use App\Client;
use App\Person;
use App\ClientType;
use Illuminate\Http\Request;
use App\Http\Requests\ClientRequest;
use Illuminate\Support\Facades\Storage;

class ClientController extends Controller
{
    /**
     * User image storage location
     *
     * @var String
     */
    protected $storage = 'public/clients';

    /**
     * Display a listing of the client.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $clients = Client::orderBy($request->order_by, $request->order)
            ->when($request->has('type'), function ($query) use ($request) {
                return $query->where('type', $request->type);
            })
            ->when($request->has('search'), function ($query) use ($request) {
                return $query->where('name', 'LIKE', "%{$request->search}%")
                    ->orWhere('dba', 'LIKE', "%{$request->search}%")
                    ->orWhere('contact_name', 'LIKE', "%{$request->search}%")
                    ->orWhere('details', 'LIKE', "%{$request->search}%");
            })
            ->paginate(25);

        $types = ClientType::all();

        return response()->json([
            'clients' => $clients,
            'types' => $types
        ], 200);
    }

    /**
     * Show the form for creating a new client.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $attorneys = Person::orderBy('last_name')->get();
        $countries = getCountries();

        return response()->json([
            'types'     => ClientType::all(),
            'attorneys' => $attorneys,
            'countries' => $countries
        ], 200);
    }

    /**
     * Store a newly created client in storage.
     *
     * @param  \App\Http\Requests\ClientRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ClientRequest $request)
    {
        $client = Client::create($request->all());

        return response()->json($client, 200);
    }

    /**
     * Display the specified client.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Client $client)
    {
        $client->load([
            'originatingAttorney',
            'billingAttorney',
            'workingAttorney',
            'type',
            'matters'
        ]);

        return response()->json($client, 200);
    }

    /**
     * Show the form for editing the specified client.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Client $client)
    {
        $attorneys = Person::orderBy('last_name')->get();
        $countries = getCountries();

        return response()->json([
            'types'     => ClientType::all(),
            'attorneys' => $attorneys,
            'client'    => $client,
            'countries' => $countries
        ], 200);
    }

    /**
     * Update the specified client in storage.
     *
     * @param  \App\Client $client
     * @param  \App\Http\Requests\ClientRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Client $client, ClientRequest $request)
    {
        if ($request->filled('number') && !$client->number) {
            $request->merge(['status' => 'exporting']);
        }

        $client->update($request->except(['image']));

        return response()->json($client, 200);
    }

    /**
     * Update the image for the specified person
     *
     * @param  \App\Client  $person
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateImage(Client $client, Request $request)
    {
        $this->validate($request, [
            'image' => 'file|mimes:jpeg,bmp,png,gif'
        ]);

        if ($client->image) {
            Storage::disk('public')
                ->delete(str_replace('/storage/', '', $client->image));
        }

        $path = $request->file('image')->store($this->storage);

        $client->image = str_replace("{$this->storage}/", '', $path);

        $client->save();

        return response()->json($client, 200);
    }

    /**
     * Remove the specified client from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Client $client)
    {
        $client->delete();

        return response()->json($client, 200);
    }
}
