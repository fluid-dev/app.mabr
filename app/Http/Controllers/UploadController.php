<?php

namespace App\Http\Controllers;

use App\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    /**
     * Uploads storage location
     *
     * @var String
     */
    protected $storage = 'public/uploads';

    /**
     * Store a newly created upload in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|file',
            'uploadable_type' => 'required|string'
        ]);

        $path = $request->file('file')->store($this->storage);

        $upload = new Upload();

        $upload->path = str_replace("{$this->storage}/", '', $path);
        $upload->name = $request->file('file')->getClientOriginalName();
        $upload->uploadable_type = $request->uploadable_type;

        $upload->save();

        return response()->json($upload, 200);
    }

    /**
     * Remove the specified upload from storage.
     *
     * @param  \App\Upload  $upload
     * @return \Illuminate\Http\Response
     */
    public function destroy(Upload $upload)
    {
        $this->delete($upload);

        return response()->json($upload, 200);
    }

    /**
     * Perform the delete action on the specified upload.
     *
     * @param  \App\Upload $upload
     */
    public function delete(Upload $upload)
    {
        Storage::disk('public')
            ->delete("/uploads/{$upload->path}");

        $upload->delete();
    }

    /**
     * Perform the delete action on an array of upload ID's.
     *
     * @param  \Illuminate\Http\Request $request
     */
    public function batchDelete(Request $request)
    {
        foreach ($request->uploads as $upload) {
            $upload = Upload::findOrFail($upload);
            $this->delete($upload);
        }
    }
}
