<?php

namespace App\Http\Controllers;

use App\Person;
use App\Location;
use App\Department;
use Illuminate\Http\Request;
use App\Events\People\PersonCreated;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\PersonCreateRequest;
use App\Http\Requests\PersonUpdateRequest;
use App\Role;

class PersonController extends Controller
{
    /**
     * User image storage location
     *
     * @var String
     */
    protected $storage = 'public/people';

    /**
     * Display a listing of the person.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $people = Person::orderBy($request->order_by, $request->order)
            ->when($request->has('role'), function ($query) use ($request) {
                return $query->where('role_id', $request->role);
            })
            ->when($request->has('location'), function ($query) use ($request) {
                return $query->where('location_id', $request->location);
            })
            ->when($request->has('search'), function ($query) use ($request) {
                return $query
                    ->where('first_name', 'LIKE', "%{$request->search}%")
                    ->orWhere('last_name', 'LIKE', "%{$request->search}%");
            })
            ->paginate(25);

        $locations = Location::orderBy('name')->get();
        $roles     = Role::orderBy('name')->get();

        return response()->json([
            'roles'     => $roles,
            'people'    => $people,
            'locations' => $locations
        ], 200);
    }

    /**
     * Show the form for creating a new person.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $departments        = Department::orderBy('name')->get();
        $locations          = Location::orderBy('name')->get();
        $people             = Person::orderBy('last_name')->get();
        $roles              = Role::orderBy('name')->get();
        $racial_identity    = racialIdentity();
        $gender_identity    = genderIdentity();

        return response()->json([
            'departments'         => $departments,
            'locations'           => $locations,
            'people'              => $people,
            'roles'               => $roles,
            'self_identification' => [
                'racial_identity'    => $racial_identity,
                'gender_identity'    => $gender_identity
            ]
        ], 200);
    }

    /**
     * Store a newly created person in storage.
     *
     * @param  \App\Http\Requests\PersonCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PersonCreateRequest $request)
    {
        $person = Person::create($request->all());

        $person->peopleSupportedBy()
            ->sync($request->people_supported_by);
        $person->peopleSupporting()
            ->sync($request->people_supporting);

        event(new PersonCreated($person));

        return response()->json($person, 200);
    }

    /**
     * Display the specified person.
     *
     * @param  Integer  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $person = Person::query()
            ->with([
                'department',
                'location',
                'selfEvaluations',
                'employeeEvaluations',
                'evaluationReviews',
                'completedEvaluations',
                'peopleSupporting',
                'peopleSupportedBy'
            ])
            ->findOrFail($id);

        return response()->json($person, 200);
    }

    /**
     * Show the form for editing the specified person.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Person $person)
    {
        $departments = Department::orderBy('name')->get();
        $locations   = Location::orderBy('name')->get();
        $people      = Person::orderBy('last_name')->get();
        $roles       = Role::orderBy('name')->get();
        $racial_identity    = racialIdentity();
        $gender_identity    = genderIdentity();

        $person->people_supporting = $person->peopleSupporting()->pluck('id');
        $person->people_supported_by = $person->peopleSupportedBy()->pluck('id');

        return response()->json([
            'departments' => $departments,
            'locations'   => $locations,
            'people'      => $people,
            'roles'       => $roles,
            'user'        => $person,
            'self_identification' => [
                'racial_identity'    => $racial_identity,
                'gender_identity'    => $gender_identity
            ]
        ], 200);
    }

    /**
     * Update the specified person in storage.
     *
     * @param  \App\Person  $person
     * @param  \App\Http\Requests\PersonUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Person $person, PersonUpdateRequest $request)
    {
        $person->update($request->except(['image', 'password']));

        if ($request->password) {
            $person->password = bcrypt($request->password);
            $person->save();
        }

        $person->peopleSupportedBy()
            ->sync($request->people_supported_by);
        $person->peopleSupporting()
            ->sync($request->people_supporting);

        return response()->json($person, 200);
    }

    /**
     * Update the image for the specified person
     *
     * @param  \App\Person  $person
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateImage(Person $person, Request $request)
    {
        $this->validate($request, [
            'image' => 'file|mimes:jpeg,bmp,png,gif'
        ]);

        if ($person->image) {
            Storage::disk('public')
                ->delete(str_replace('/storage/', '', $person->image));
        }

        $path = $request->file('image')->store($this->storage);

        $person->image = str_replace("{$this->storage}/", '', $path);

        $person->save();

        return response()->json($person, 200);
    }

    /**
     * Remove the specified person from storage.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Person $person)
    {
        $person->peopleSupporting()->detach();
        $person->peopleSupportedBy()->detach();
        $person->delete();

        return response()->json($person, 200);
    }
}
