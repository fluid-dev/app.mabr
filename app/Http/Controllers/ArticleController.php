<?php

namespace App\Http\Controllers;

use App\Person;
use App\Upload;
use App\Article;
use App\Category;
use Illuminate\Http\Request;

use App\Http\Requests\ArticleRequest;
use Illuminate\Support\Facades\Storage;

class ArticleController extends Controller
{
    /**
     * Display a listing of the article.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $articles = Article::published()
            ->with('category')
            ->when($request->has('category'), function ($query) use ($request) {
                return $query->where('category_id', $request->category);
            })
            ->when($request->has('search'), function ($query) use ($request) {
                return $query
                    ->where('title', 'LIKE', "%{$request->search}%")
                    ->orWhere('content', 'LIKE', "%{$request->search}%")
                    ->orWhereHas('author', function ($query) use ($request) {
                        return $query
                            ->where('first_name', 'LIKE', "%{$request->search}%")
                            ->orWhere('last_name', 'LIKE', "%{$request->search}%");
                    });
            })
            ->paginate(20);

        $categories = Category::for('article');

        return response()->json([
            'articles' => $articles,
            'categories' => $categories
        ], 200);
    }

    /**
     * Show the form for creating a new article.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $attorneys = Person::orderBy('last_name')->get();

        return response()->json([
            'categories' => Category::for('article'),
            'attorneys'  => $attorneys
        ], 200);
    }

    /**
     * Store a newly created article in storage.
     *
     * @param  \App\Http\Requests\ArticleRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ArticleRequest $request)
    {
        if ($request->has('add_category')) {
            $request->merge([
                'category_id' => Category::add($request->category_id, 'article')
            ]);
        }

        $article = Article::create($request->all());

        $article->relatedAttorneys()->attach($request->related_attorneys);

        if ($request->uploads) {
            $this->attachUploads($request->uploads, $article);
        }

        return response()->json($article, 200);
    }

    /**
     * Display the specified article.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Article $article)
    {
        $article->load('relatedAttorneys', 'uploads', 'category');

        return response()->json($article, 200);
    }

    /**
     * Show the form for editing the specified article.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Article $article)
    {
        $attorneys = Person::orderBy('last_name')->get();

        $article->load('uploads');

        $article->related_attorneys = $article->relatedAttorneys()->pluck('id');

        return response()->json([
            'article' => $article,
            'categories' => Category::for('article'),
            'attorneys'  => $attorneys
        ], 200);
    }

    /**
     * Update the specified article in storage.
     *
     * @param  \App\Http\Requests\ArticleRequest  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ArticleRequest $request, Article $article)
    {
        if ($request->has('add_category')) {
            $request->merge([
                'category_id' => Category::add($request->category_id, 'article')
            ]);
        }

        $article->update($request->all());

        $article->relatedAttorneys()->sync($request->related_attorneys);

        if ($request->uploads) {
            $this->attachUploads($request->uploads, $article);
        }

        return response()->json($article, 200);
    }

    /**
     * Remove the specified article from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Article $article)
    {
        $this->detachUploads($article->uploads);

        $article->delete();

        return response()->json($article, 200);
    }

    /**
     * Attach the article to the uploaded files
     *
     * @param  array  $uploads
     * @param  \App\Article  $article
     * @return void
     */
    public function attachUploads(array $uploads, Article $article)
    {
        foreach ($uploads as $id) {
            $upload = Upload::findOrFail($id);
            $upload->uploadable_id = $article->id;
            $upload->save();
        }
    }

    /**
     * Delete the uploads associated to the Article
     *
     * @param  array $uploads
     * @return void
     */
    public function detachUploads($uploads)
    {
        foreach ($uploads as $index => $item) {
            $upload = Upload::findOrFail($item->id);
            Storage::disk('public')
                ->delete("/uploads/{$upload->path}");
            $upload->delete();
        }
    }
}
