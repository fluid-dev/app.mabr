<?php

namespace App\Http\Controllers;

use App\Department;

use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the department.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::orderBy('name')->paginate(25);

        return response()->json([
            'departments' => $departments
        ], 200);
    }

    /**
     * Store a newly created department in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $department = new Department();

        $department->name = $request->name;
        $department->slug = str_slug($request->name);
        $department->save();

        return response()->json($department, 200);
    }

    /**
     * Update the specified department in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        $department->name = $request->name;
        $department->slug = str_slug($request->name);
        $department->save();

        return response()->json($department, 200);
    }

    /**
     * Remove the specified department from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        $department->delete();

        return response()->json($department, 200);
    }
}
