<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'billing_address'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'type' => 'integer'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'name',
        'number',
        'status',
        'dba',
        'contact_name',
        'street_address',
        'city',
        'state',
        'zip',
        'country',
        'contact_phone',
        'billing_phone',
        'contact_email',
        'billing_email',
        'originating_attorney',
        'billing_attorney',
        'working_attorney',
        'details',
        'image'
    ];

    protected static function boot()
    {
        parent::boot();

        self::deleting(function (Client $client) {
            foreach ($client->matters as $matter) {
                $matter->delete();
            }
        });
    }

    public function getBillingAddressAttribute()
    {
        return "{$this->street_address} {$this->city} {$this->state}, {$this->zip}";
    }

    public function getImageAttribute($path)
    {
        if ($path) {
            return "/storage/clients/{$path}";
        }
    }

    public function type()
    {
        return $this->belongsTo(ClientType::class, 'type');
    }

    public function originatingAttorney()
    {
        return $this->belongsTo(Person::class, 'originating_attorney');
    }

    public function billingAttorney()
    {
        return $this->belongsTo(Person::class, 'billing_attorney');
    }

    public function workingAttorney()
    {
        return $this->belongsTo(Person::class, 'working_attorney');
    }

    public function matters()
    {
        return $this->hasMany(Matter::class);
    }
}
