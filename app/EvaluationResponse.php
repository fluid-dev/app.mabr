<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluationResponse extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'evaluation_id',
        'evaluator_id',
        'response_id',
        'response'
    ];

    public function evaluator()
    {
        return $this->belongsTo(Person::class, 'evaluator_id');
    }
}
