<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Validation\Rule;

class ValidResetToken implements Rule
{
    protected $request;

    /**
     * Create a new rule instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $oneHourAgo = Carbon::now()->subHours(1)->toDateTimeString();

        $reset = DB::table('password_resets')
                   ->where('email', $this->request->email)
                   ->where('created_at', '>', $oneHourAgo)
                   ->first();

        if ($reset) {
            return Hash::check($value, $reset->token);
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The password reset link has expired or is no longer valid. Try resetting your password again';
    }
}
