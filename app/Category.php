<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'type'
    ];

    public static function add($name, $type)
    {
        $category = (new static)->create([
            'name' => $name,
            'type' => $type
        ]);

        return $category->id;
    }

    public function scopeFor($query, $type)
    {
        return $query->where('type', $type)->orderBy('name')->get();
    }
}
