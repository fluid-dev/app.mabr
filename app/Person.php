<?php

namespace App;

use Carbon\Carbon;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Person extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'full_name', 'is_auth'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'hire_date',
        'birthday'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'title',
        'hire_date',
        'birthday',
        'role_id',
        'department_id',
        'location_id',
        'office',
        'cell_phone',
        'direct_phone',
        'image',
        'quick_links',
        'address',
        'public_address',
        'bar_numbers',
        'emergency_contact',
        'emergency_phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function getIsAuthAttribute()
    {
        return $this->id === auth()->id();
    }

    public function setQuickLinksAttribute($value)
    {
        $this->attributes['quick_links'] = serialize($value);
    }

    public function getQuickLinksAttribute($value)
    {
        return unserialize($value);
    }

    public function getImageAttribute($path)
    {
        if ($path) {
            return "/storage/people/{$path}";
        }
    }

    public function setHireDateAttribute($value)
    {
        return $this->attributes['hire_date'] = Carbon::parse($value);
    }

    public function setBirthdayAttribute($value)
    {
        return $this->attributes['birthday'] = Carbon::parse($value);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function peopleSupporting()
    {
        return $this->belongsToMany(Person::class, 'person_support', 'supporting_id', 'supported_id');
    }

    public function peopleSupportedBy()
    {
        return $this->belongsToMany(Person::class, 'person_support', 'supported_id', 'supporting_id');
    }

    public function articles()
    {
        return $this->hasMany(Article::class, 'author_id');
    }

    public function selfEvaluations()
    {
        return $this->hasMany(Evaluation::class)
            ->where('self_eval', null);
    }

    public function completedEvaluations()
    {
        return $this->hasMany(Evaluation::class)
            ->where('acknowledged', '!=', null);
    }

    public function evaluationReviews()
    {
        return $this->hasMany(Evaluation::class)
            ->where('self_eval', '!=', null)
            ->where('group_eval', '!=', null)
            ->where('approved', '!=', null)
            ->where('acknowledged', null);
    }

    public function employeeEvaluations()
    {
        return $this->belongsToMany(Evaluation::class, 'evaluation_contributors')
            ->where('self_eval', '!=', null)
            ->wherePivot('completed', null);
    }

    public function hasRole($role)
    {
        if (is_array($role)) {
            return in_array($this->role->slug, $role);
        }

        return $this->role->slug === $role;
    }
}
