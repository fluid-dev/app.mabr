<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Resource extends Model
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'date:F j, Y'
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'type', 'path'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['category'];

    public function getPathAttribute($path)
    {
        if ($this->source == 'url') {
            return $path;
        }

        return "/resources/{$this->type}/{$path}";
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }
}
