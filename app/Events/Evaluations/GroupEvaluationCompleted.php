<?php

namespace App\Events\Evaluations;

use App\Evaluation;
use Illuminate\Queue\SerializesModels;

class GroupEvaluationCompleted
{
    use SerializesModels;

    public $evaluation;

    /**
     * Create a new event instance.
     *
     * @param Evaluation $evaluation App\Evaluation
     * @return void
     */
    public function __construct(Evaluation $evaluation)
    {
        $this->evaluation = $evaluation;
    }
}
