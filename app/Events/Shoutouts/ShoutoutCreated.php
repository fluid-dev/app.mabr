<?php

namespace App\Events\Shoutouts;

use App\Shoutout;
use Illuminate\Queue\SerializesModels;

class ShoutoutCreated
{
    use SerializesModels;

    public $shoutout;

    /**
     * Create a new event instance.
     *
     * @param Shoutout $shoutout App\Shouout
     * @return void
     */
    public function __construct(Shoutout $shoutout)
    {
        $this->shoutout = $shoutout;
    }
}
