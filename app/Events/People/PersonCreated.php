<?php

namespace App\Events\People;

use App\Person;
use Illuminate\Queue\SerializesModels;

class PersonCreated
{
    use SerializesModels;

    public $user;

    /**
     * Create a new event instance.
     *
     * @param Person $user App\Person
     * @return void
     */
    public function __construct(Person $user)
    {
        $this->user = $user;
    }
}
