<?php

namespace App;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message',
        'person_id'
    ];

    public function scopeRecent($query)
    {
        return $query->where('created_at', '>=', Carbon::now()->subMonth());
    }

    public function person()
    {
        return $this->belongsTo(Person::class);
    }
}
