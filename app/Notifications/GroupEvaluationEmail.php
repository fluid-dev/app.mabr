<?php

namespace App\Notifications;

use App\Evaluation;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class GroupEvaluationEmail extends Notification
{
    use Queueable;

    protected $evaluation;

    /**
     * Create a new notification instance.
     *
     * @param Evaluation $evaluation App\Evaluation
     * @return void
     */
    public function __construct(Evaluation $evaluation)
    {
        $this->evaluation = $evaluation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Employee Evaluation')
                    ->greeting('Hey, there!')
                    ->line("An evaluation for {$this->evaluation->employee->full_name} is awaiting your input. Please, click the button below to be taken to the evaluation now.")
                    ->action('Show Evaluation', url("/evaluations/{$this->evaluation->id}/group"))
                    ->line('Thanks!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
