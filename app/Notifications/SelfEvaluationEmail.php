<?php

namespace App\Notifications;

use App\Evaluation;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SelfEvaluationEmail extends Notification
{
    use Queueable;

    protected $evaluation;

    /**
     * Create a new notification instance.
     *
     * @param Evaluation $evaluation \App\Evaluation
     * @return void
     */
    public function __construct(Evaluation $evaluation)
    {
        $this->evaluation = $evaluation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Self Evaluation')
            ->greeting('Hey, there!')
            ->line('A self evaluation has been created for you to complete. Visit your account to get started or click the button below to be taken there.')
            ->action('Self Evaluation', url("/evaluations/{$this->evaluation->id}/self"))
            ->line('Thanks!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
