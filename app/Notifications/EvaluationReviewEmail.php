<?php

namespace App\Notifications;

use App\Evaluation;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class EvaluationReviewEmail extends Notification
{
    use Queueable;

    protected $evaluation;

    /**
     * Create a new notification instance.
     *
     * @param Evaluation $evaluation App\Evaluation
     * @return void
     */
    public function __construct(Evaluation $evaluation)
    {
        $this->evaluation = $evaluation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Evaluation Approval')
            ->greeting('Hey, there!')
            ->line('An evaluation has been completed for ' . $this->evaluation->employee->full_name . '. You may now review this evaluation and export the details of it to a CSV file.')
            ->action('Show Evaluation', url("/evaluations/{$this->evaluation->id}"))
            ->line('Thanks!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
