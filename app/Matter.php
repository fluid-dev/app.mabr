<?php

namespace App;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Matter extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['target_filing_date', 'deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'type_name',
        'type_class',
        'title',
        'responsible_attorney',
        'working_attorney',
        'remarks',
        'inventors',
        'reference_number',
        'patent_country',
        'patent_type',
        'patent_application_type',
        'target_filing_date',
        'mark',
        'class',
        'mark_country',
        'narrative_summary',
        'adverse_party',
        'representation',
        'keywords'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'client',
        'responsibleAttorney',
        'workingAttorney'
    ];

    public function setTargetFilingDateAttribute($value)
    {
        if ($value) {
            return $this->attributes['target_filing_date'] = Carbon::parse($value);
        }
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function responsibleAttorney()
    {
        return $this->belongsTo(Person::class, 'responsible_attorney');
    }

    public function workingAttorney()
    {
        return $this->belongsTo(Person::class, 'working_attorney');
    }
}
