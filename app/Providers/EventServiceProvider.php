<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            'SocialiteProviders\\Azure\\AzureExtendSocialite@handle',
        ],
        'App\Events\People\PersonCreated' => [
            'App\Listeners\CreateUserPasswordAndSendEmail',
        ],
        'App\Events\Evaluations\EvaluationCreated' => [
            'App\Listeners\SendSelfEvaluationEmail'
        ],
        'App\Events\Evaluations\SelfEvaluationCompleted' => [
            'App\Listeners\SendGroupEvaluationEmail'
        ],
        'App\Events\Evaluations\GroupEvaluationCompleted' => [
            'App\Listeners\DetermineGroupEvaluationStatus'
        ],
        // 'App\Events\Evaluations\EvaluationApproved' => [
        //     'App\Listeners\SendApprovedEvaluationEmail'
        // ],
        'App\Events\Shoutouts\ShoutoutCreated' => [
            'App\Listeners\SendShoutoutApprovalEmail'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
