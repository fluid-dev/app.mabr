<?php

namespace App\Listeners;

use App\Events\People\PersonCreated;
use App\Notifications\UserPasswordEmail;

use Illuminate\Contracts\Queue\ShouldQueue;

class CreateUserPasswordAndSendEmail implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  PersonCreated  $event
     * @return void
     */
    public function handle(PersonCreated $event)
    {
        $password = str_random();

        $event->user->password = bcrypt($password);

        $event->user->save();

        $event->user->notify(new UserPasswordEmail($password));
    }
}
