<?php

namespace App\Listeners;

use App\Notifications\GroupEvaluationEmail;
use App\Events\Evaluations\SelfEvaluationCompleted;

use Illuminate\Contracts\Queue\ShouldQueue;

class SendGroupEvaluationEmail implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  SelfEvaluationCompleted  $event
     * @return void
     */
    public function handle(SelfEvaluationCompleted $event)
    {
        foreach ($event->evaluation->contributors as $contributor) {
            $contributor->notify(new GroupEvaluationEmail($event->evaluation));
        }
    }
}
