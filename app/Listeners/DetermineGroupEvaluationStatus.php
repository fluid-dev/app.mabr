<?php

namespace App\Listeners;

use App\Setting;
use App\Notifications\EvaluationReviewEmail;
use App\Events\Evaluations\GroupEvaluationCompleted;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class DetermineGroupEvaluationStatus implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  GroupEvaluationCompleted  $event
     * @return void
     */
    public function handle(GroupEvaluationCompleted $event)
    {
        $evaluation       = $event->evaluation;
        $all_contributors = $evaluation->contributors->count();
        $completed        = 0;

        foreach ($evaluation->contributors as $contributor) {
            if ($contributor->pivot->completed) {
                $completed++;
            }
        }

        if ($all_contributors === $completed) {
            $evaluation->group_eval = now();

            $evaluation->save();

            $setting = Setting::byKey('notifications');

            Notification::route('mail', $setting['evaluation_email'])->notify(new EvaluationReviewEmail($event->evaluation));
        }
    }
}
