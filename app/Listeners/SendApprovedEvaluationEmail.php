<?php

namespace App\Listeners;

use App\Events\Evaluations\EvaluationApproved;

use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\EvaluationApprovedEmail;

class SendApprovedEvaluationEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EvaluationApproved  $event
     * @return void
     */
    public function handle(EvaluationApproved $event)
    {
        $event->evaluation->employee->notify(new EvaluationApprovedEmail($event->evaluation));
    }
}
