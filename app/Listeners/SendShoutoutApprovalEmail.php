<?php

namespace App\Listeners;

use App\Person;

use App\Notifications\ShoutoutApprovalEmail;

use App\Events\Shoutouts\ShoutoutCreated;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendShoutoutApprovalEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ShoutoutCreated  $event
     * @return void
     */
    public function handle(ShoutoutCreated $event)
    {
        $recipients = Person::whereHas('role', function ($query) {
            return $query->where('slug', 'marketing');
        })->get();

        foreach ($recipients as $recipient) {
            $recipient->notify(new ShoutoutApprovalEmail($event->shoutout->id));
        }
    }
}
