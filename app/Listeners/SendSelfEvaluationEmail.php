<?php

namespace App\Listeners;

use App\Notifications\SelfEvaluationEmail;
use App\Events\Evaluations\EvaluationCreated;

use Illuminate\Contracts\Queue\ShouldQueue;

class SendSelfEvaluationEmail implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  EvaluationCreated  $event
     * @return void
     */
    public function handle(EvaluationCreated $event)
    {
        $event->evaluation->employee->notify(new SelfEvaluationEmail($event->evaluation));
    }
}
