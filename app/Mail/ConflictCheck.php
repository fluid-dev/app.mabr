<?php

namespace App\Mail;

use App\Matter;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConflictCheck extends Mailable
{
    use Queueable, SerializesModels;

    public $matter;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Matter $matter)
    {
        $matter->load('workingAttorney', 'responsibleAttorney');

        $this->matter = $matter;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('New Conflict Check')
            ->markdown('emails.matters.conflict-check');
    }
}
