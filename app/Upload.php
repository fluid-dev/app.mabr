<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'path',
        'uploadable_id',
        'uploadable_type'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['link'];

    public function uploadable()
    {
        return $this->morphTo();
    }

    public function getLinkAttribute()
    {
        if ($this->path) {
            return "/storage/uploads/{$this->path}";
        }
    }
}
