<?php

namespace App;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class Shoutout extends Model
{
    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => 'App\Events\Shoutouts\ShoutoutCreated'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message',
        'anonymous'
    ];

    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    public function scopeRecent($query)
    {
        return $query->where('approved', '>=', Carbon::now()->subMonth());
    }

    public function scopeApproved($query)
    {
        return $query->where('approved', '!=', null);
    }
}
