<?php

namespace App;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'questionnaire',
        'results',
        'comments',
        'average_score'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'type' => 'integer'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'period_start',
        'period_end',
        'acknowledged'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'person_id',
        'period_start',
        'period_end',
        'type',
        'self_eval',
        'acknowledged'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'employee',
        'contributors'
    ];

    public function setPeriodStartAttribute($value)
    {
        return $this->attributes['period_start'] = Carbon::parse($value);
    }

    public function setPeriodEndAttribute($value)
    {
        return $this->attributes['period_end'] = Carbon::parse($value);
    }

    public function setSelfEvalAttribute($value)
    {
        $this->attributes['self_eval'] = serialize($value);
    }

    public function getSelfEvalAttribute($value)
    {
        return unserialize($value);
    }

    public function getQuestionnaireAttribute()
    {
        $items = $this->evaluatorResponses;
        $questionnaire = new \StdClass();

        foreach ($items as $item) {
            $questionnaire->{$item->response_id} = $item->response;
        }

        return $questionnaire;
    }

    public function getResultsAttribute()
    {
        $items = $this->responses;
        $results = [];

        foreach ($items as $item) {
            if (strpos($item->response_id, 'comment') === false) {
                if ($item->response != 0) { // Ignore N/A Values
                    $results[$item->response_id][] = (float)$item->response;
                }
            }
        }

        $averages = [];

        foreach ($results as $id => $values) {
            $responses = count($values);

            $average = (array_sum($values) / $responses);

            $averages[$id] = $average;
        }

        return $averages;
    }

    public function getCommentsAttribute()
    {
        $items = $this->responses;
        $comments = [];

        foreach ($items as $item) {
            if (strpos($item->response_id, 'comment') !== false) {
                $comments[$item->response_id][] = [
                    'comment' => $item->response,
                    'name' => $item->evaluator->full_name
                ];
            }
        }

        return $comments;
    }

    public function getAverageScoreAttribute()
    {
        $results = $this->results;

        $score = 0;
        $count = 0;

        foreach ($results as $key => $value) {
            $score = $score + $value;
            $count++;
        }

        if ($count) {
            return number_format($score / $count, 3);
        }
    }

    public function employee()
    {
        return $this->belongsTo(Person::class, 'person_id');
    }

    public function contributors()
    {
        return $this->belongsToMany(Person::class, 'evaluation_contributors')
            ->withPivot('completed');
    }

    public function responses()
    {
        return $this->hasMany(EvaluationResponse::class);
    }

    public function evaluatorResponses()
    {
        return $this->hasMany(EvaluationResponse::class)
            ->where('evaluator_id', auth()->id());
    }
}
