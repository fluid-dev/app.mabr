<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * The attributes appended to the returned model.
     *
     * @var array
     */
    protected $appends = [
        'excerpt', 'big_excerpt'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['published_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'category_id',
        'content',
        'author_id',
        'published_at'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'author'
    ];

    public function getExcerptAttribute()
    {
        return trim(substr(rtrim(strip_tags($this->content), '.'), 0, 135)) . '&hellip;';
    }

    public function getBigExcerptAttribute()
    {
        return trim(substr(rtrim(strip_tags($this->content), '.'), 0, 350)) . '&hellip;';
    }

    public function setPublishedAtAttribute($value)
    {
        return $this->attributes['published_at'] = Carbon::parse($value);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function author()
    {
        return $this->belongsTo(Person::class, 'author_id');
    }

    public function uploads()
    {
        return $this->morphMany(Upload::class, 'uploadable');
    }

    public function relatedAttorneys()
    {
        return $this->belongsToMany(Person::class, 'article_person', 'article_id', 'person_id');
    }

    public function scopePublished($query)
    {
        return $query
            ->where('published_at', '<=', Carbon::now()->startOfDay())
            ->orderByDesc('published_at')
            ->orderByDesc('created_at');
    }
}
