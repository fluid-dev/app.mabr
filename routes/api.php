<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function ($router) {
    Route::post('login', 'Auth\LoginController@login')
        ->name('auth.login');

    Route::post('password/remind', 'Auth\ForgotPasswordController@sendResetLinkEmail')
        ->name('auth.remind');

    Route::post('password/reset', 'Auth\ResetPasswordController@reset')
        ->name('auth.reset');

    Route::middleware('auth:api')->group(function ($router) {
        Route::post('logout', 'Auth\LoginController@logout')
            ->name('auth.logout');

        Route::get('refresh', 'Auth\LoginController@refresh')
            ->name('auth.refresh');

        Route::get('user', 'Auth\LoginController@user')
            ->name('auth.user');
    });
});

Route::post('oauth/callback/{provider}', 'Auth\LoginController@handleProviderCallback');

Route::middleware('auth:api')->group(function () {
    Route::get('dashboard', 'AppController@dashboard');
    Route::get('navbar', 'AppController@navbar');

    Route::post('search', 'SearchController@results');

    Route::resource('clients', 'ClientController');
    Route::post('clients/{client}/image', 'ClientController@updateImage');
    Route::put('clients/{client}/approve', 'ClientProcessController@approve');
    Route::put('clients/{client}/reject', 'ClientProcessController@reject');
    Route::get('clients/{client}/anaqua', 'ClientProcessController@anaquaCsv');
    Route::get('clients/{client}/imanage', 'ClientProcessController@iManageCsv');

    Route::resource('matters', 'MatterController');

    Route::post('people/{person}/image', 'PersonController@updateImage');
    Route::post('people/{person}/restore', 'PersonController@restore');
    Route::resource('people', 'PersonController');

    Route::resource('evaluations', 'EvaluationController');
    Route::put('evaluations/{evaluation}/self', 'EvaluationController@selfEvaluation');
    Route::put('evaluations/{evaluation}/group', 'EvaluationController@groupEvaluation');
    Route::put('evaluations/{evaluation}/approve', 'EvaluationController@approveEvaluation');
    Route::get('evaluations/{evaluation}/export', 'EvaluationController@exportEvaluation');

    Route::resource('resources', 'ResourceController')
        ->except([
            'show',
            'edit',
            'update'
        ]);

    Route::resource('articles', 'ArticleController');

    Route::delete('uploads/batch', 'UploadController@batchDelete');
    Route::resource('uploads', 'UploadController')
        ->only([
            'store',
            'destroy'
        ]);

    Route::resource('settings', 'SettingController')
        ->only([
            'index',
            'store'
        ]);

    Route::resource('announcements', 'AnnouncementController')
        ->except(['create', 'show', 'edit']);

    Route::resource('shoutouts', 'ShoutoutController')
        ->except(['create', 'show', 'edit']);
    Route::post('shoutouts/{shoutout}/approve', 'ShoutoutController@approve');

    Route::resource('locations', 'LocationController')
        ->except(['create', 'show', 'edit']);

    Route::resource('departments', 'DepartmentController')
        ->except(['create', 'show', 'edit']);

    Route::resource('categories', 'CategoryController')
        ->except(['create', 'show', 'edit']);
});

// Should probably figure out a way to guard this
Route::resource('editor', 'EditorController')
    ->only([
        'index',
        'store',
    ]);
Route::delete('editor', 'EditorController@destroy');
