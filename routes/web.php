<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test', function () {
    //
});

Route::view('/{any?}', 'app')
    ->where('any', '.*')
    ->name('home');

Route::view('password/reset/{token?}', 'app')
    ->name('password.reset');

Route::view('login', 'app')
    ->name('login');
